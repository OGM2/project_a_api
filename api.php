<?php 

namespace ProjectA;

use ProjectA\Core\Bootstrap;
use ProjectA\Classes\Router;

include_once 'core/bootstrap.php';

Bootstrap::load();

$router = new Router();
$router->getEndpoint();