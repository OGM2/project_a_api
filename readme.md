# Project A API

API endpoint for newsletter registration.

## How it works

I developed this endpoint on my localhost and used .htaccess to redirect all incoming requests to api.php where they are further distributed. Included is dump of database structure in sql file. I used Dibi database layer (https://dibiphp.com/) for interaction with database. Dibi takes care of possible SQL injections if used properly. I used Postman to send requests.

Newsletter registration endpoint is located at */api/newsletter/register* and accepts POST requests in form of JSON.

```
{
	"email":"example@gmail.cz"
}
```

Email is validated and saved to database. 

I also included endpoint for registration confirmation since the email is inserted as unconfirmed. This endpoint is located at */api/newsletter/confirm* and accepts PUT requests in the same form.

API returns responses in form of JSON with status code and data relevant for the request made.