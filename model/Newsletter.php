<?php

namespace ProjectA\Model;

use ProjectA\Model\Parent\Model;

class Newsletter extends Model
{
	public function saveEmail($email)
	{
		$emailQuery = $this->db->query('SELECT email FROM project_a_newsletter WHERE email = ?', $email);

		if($emailQuery->getRowCount() == 1)
		{
			return false;
		}

		$arr = array(
			'email' => $email,
			'confirmed' => false,
			'date_created' => date('Y-m-d H:i:s'),
			'date_updated' => date('Y-m-d H:i:s'),
		);

		if($this->db->query('INSERT INTO project_a_newsletter %v', $arr))
		{
			$arr['id'] = $this->db->getInsertId();

			return $arr;
		}
	}

	public function confirmEmail($email)
	{
		$emailQuery = $this->db->query('SELECT * FROM project_a_newsletter WHERE email = ?', $email);

		if($emailQuery->getRowCount() == 1)
		{
			$userEmail = $emailQuery->fetch();
			
			if($userEmail->confirmed == 0)
			{
				$arr = array(
					'confirmed' => 1,
					'date_updated' => date('Y-m-d H:i:s'),
				);

				$this->db->query('UPDATE project_a_newsletter SET', $arr, 'WHERE email = ?', $email);
			}
		}
		
		return (isset($arr) && !empty($arr)) ? $arr : false;
	}
}