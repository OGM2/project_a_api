<?php 

namespace ProjectA\Model\Parent;

use Dibi\Connection;

class Model
{
	protected $db;
	
	public function __construct()
	{
		include_once 'core/config.php';

		$this->db = new Connection($conf['db']);
	}
}