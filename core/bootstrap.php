<?php

namespace ProjectA\Core;

class Bootstrap 
{
	
	public static function load()
	{

		// load classes

		foreach(glob('class/parent/*.php') as $parentClass)
		{
			include $parentClass;
		}

		foreach(glob('class/*.php') as $class)
		{
			include $class;
		}

		// load models

		foreach(glob('model/parent/*.php') as $parentModel)
		{
			include $parentModel;
		}

		foreach(glob('model/*.php') as $model)
		{
			include $model;
		}

		// load third party bundles

		include_once 'vendor/autoload.php';

	}
	
}
