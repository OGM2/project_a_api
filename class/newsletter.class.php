<?php

namespace ProjectA\Classes;

use ProjectA\Classes\Parent\Api;
use ProjectA\Model\Newsletter;

class NewsletterController extends Api
{
	public $newsletter;

	public function __construct()
	{
		parent::__construct();
		$this->newsletter = new Newsletter();
	}

	public function getMethod($method)
	{
		if($method == 'register')
		{
			$_SERVER['REQUEST_METHOD'] == 'POST' ?: $this->response(405, 'Method Not Allowed', NULL);
			
			$this->registerEmail();
		}
		elseif($method == 'confirm')
		{
			$_SERVER['REQUEST_METHOD'] == 'PUT' ?: $this->response(405, 'Method Not Allowed', NULL);
			$this->confirmEmail();
		}
		else
		{
			$this->response(404, 'Not a valid API endpoint', NULL);
		}
	}

	public function registerEmail()
	{
		filter_var($this->request['email'], FILTER_VALIDATE_EMAIL) ?: $this->response(400, 'Bad Request', NULL);

		$response = $this->newsletter->saveEmail($this->request['email']);

		if(isset($response) && !empty($response))
		{
			$this->response(201, 'Created', $response);
		}
		else
		{
			$this->response(400, 'Bad Request', NULL);	
		} 
	}

	public function confirmEmail()
	{
		isset($this->request['email']) && !empty($this->request['email']) ?: $this->response(400, 'Bad Request', NULL);

		$response = $this->newsletter->confirmEmail($this->request['email']);
	
		if($response != false)
		{
			$this->response(200, 'Email Confirmed', $data = array(
				'email' => $this->request['email'],
				'date_updated' => $response['date_updated'],
			));
		}
		else
		{
			$this->response(400, 'Bad Request', NULL);
		}
	}

	// place for other methods e.g. updateEmail, deleteEmail etc.

}