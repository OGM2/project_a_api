<?php
namespace ProjectA\Classes;

use ProjectA\Classes\Parent\Api;
use ProjectA\Classes\NewsletterController;

class Router extends Api
{
	public function getEndpoint()
	{

		$url = explode('/',trim($_SERVER['REQUEST_URI'],'/'));
		
		if($url[0] == 'api' && $url[1] == 'newsletter' && !empty($url[2]))
		{
			$newsletter = new NewsletterController();
			$newsletter->getMethod($url[2]);
		}
		else
		{
			$this->response(404, 'Not a valid API endpoint', NULL);
			exit;
		}

		// place for other endpoints e.g. user, comment, article etc.
	}
}