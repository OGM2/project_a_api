<?php

namespace ProjectA\Classes\Parent;

class Api 
{
	public $request;

	public function __construct()
	{
		$this->request = json_decode(file_get_contents('php://input'), true);
	}

	public function response($status, $statusMessage, $data)
	{
		header("HTTP/1.1 ".$status);

		$response['status'] = $status;
		$response['statusMessage'] = $statusMessage;
		$response['data'] = $data;

		$json_response = json_encode($response);

		echo $json_response;

		exit;
	}
}